import React, {useEffect} from 'react';
import './App.css';

function App() {

  const receiveMessage = (event) => {
    console.log(event.data, '11111111111111111')
  }

  useEffect(() => {
    window.addEventListener("message", receiveMessage);
  }, [])

  const onClick = () => {
    document.getElementById('target').contentWindow.postMessage({ type: 'intoIframe', event: 'intoIframe', data: ['123intoIframe', '321intoIframe'] }, '*');
  }
  return (
    <div>
      <button onClick={onClick}>1234567</button>
      <iframe style={{width: '100%', height: '100vh'}} src="http://localhost:3000" id="target" />
    </div>
  );
}

export default App;
